@extends('layouts.admin') @section('content') @if(Session::has('deleted_post'))
<p class="alert alert-danger">{{ session('deleted_post') }}</p>
@endif
<h1>Posts</h1>
<table class="table">
	<thead>
		<tr>
			<th>Id</th>
			<th>User</th>
			<th>Photo</th>
			<th>Category</th>
			<th>Title</th>
			<th>Body</th>
			<th>View Post</th>
			<th>View Comments</th>
			<th>Created</th>
			<th>Updated</th>
		</tr>
	</thead>
	<tbody>
		@if($posts) @foreach($posts as $post)
		<tr>
			<td>{{$post->id}}</td>
			<td>{{ $post->user->name}}</td>
			<td>
				@if($post->photo)
				<img src="{{ asset($post->photo->file) }} " height="50px" /> @else
				<img src="http://placehold.it/50x50" /> @endif
			</td>
			<td>{{ $post->category != null ? $post->category->name : 'uncategorized'}}</td>
			<td>
				<a title="edit post" href="{{ route('posts.edit', $post->id) }}">{{ $post->title}}</a>
			</td>
			<td>{!! str_limit(strip_tags($post->body), 30) !!}</td>
			<td>
				<a href="{{ route('view.post', $post->id) }}">View Post</a>
			</td>
			<td>
				<a href="{{route('comments.show', $post->id)}}">View Comment</a>
			</td>

			<td>{{$post->created_at->diffForHumans()}}</td>
			<td>{{$post->updated_at->diffForHumans()}}</td>
		</tr>
		@endforeach @endif
	</tbody>
</table>
<div class="row">
	<div class="col-sm-6 col-sm-offset-5">
		{{$posts->render()}}
	</div>
</div>
@endsection