@extends('layouts.admin') @section('content')
 @if(Session::has('deleted_user')) 
 <p class="alert alert-danger">{{ session('deleted_user') }}</p>
  @endif
<h1>Users</h1>
<table class="table">
	<thead>
		<tr>
			<th>Id</th>
			<th>Photo</th>
			<th>Name</th>
			<th>Email</th>
			<th>Role</th>
			<th>Status</th>
			<th>Created</th>
			<th>Updated</th>
		</tr>
	</thead>
	<tbody>
		@if($users) @foreach($users as $user)
		<tr>
			<td>{{$user->id}}</td>
			<td>@if($user->photo)
				<img src="{{ asset($user->photo->file) }} " height="50px" /> @endif 
				<img src="{{ asset($user->gravatar) }} " height="50px" />
			</td>
			<td>
				<a href="{{ route('users.edit', $user->id) }}">{{$user->name}}</a>
			</td>
			<td>{{$user->email}}</td>
			<td>{{ $user->role != null ? $user->role->name : 'no role'}}</td>
			<td>{{$user->is_active == 1 ? 'Active': 'Not active'}}</td>
			<td>{{$user->created_at->diffForHumans()}}</td>
			<td>{{$user->updated_at->diffForHumans()}}</td>
		</tr>
		@endforeach @endif
	</tbody>
</table>
@endsection