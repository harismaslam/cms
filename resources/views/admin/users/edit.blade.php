@extends('layouts.admin') @section('content')
<div class="col-sm-12">
	<h1>Edit User</h1>
</div>

<div class="row">
	<div class="col-sm-3">
		<img src="{{ !empty($user->photo)? asset($user->photo->file) : 'http://placehold.it/400x400' }}" class="img-responsive img-rounded"
		/>
	</div>
	<div class="col-sm-9">
		{!! Form::model($user, ['method'=>'PATCH', 'action'=>['AdminUsersController@update', $user->id], 'files'=>true]) !!}

		<div class="form-group">
			{!! Form::label('name', 'Name') !!} {!! Form::text('name', null, ['class'=>'form-control']) !!}
		</div>
		<div class="form-group">
			{!! Form::label('email', 'Email') !!} {!! Form::email('email', null, ['class'=>'form-control']) !!}
		</div>
		<div class="form-group">
			{!! Form::label('role_id', 'Role') !!} {!! Form::select('role_id', [''=>'Choose an Option'] + $roles, null, ['class'=>'form-control'])
			!!}
		</div>
		<div class="form-group">
			{!! Form::label('photo', 'Image') !!} {!! Form::file('photo', null, ['class'=>'form-control']) !!}
		</div>
		<div class="form-group">
			{!! Form::label('is_active', 'Status') !!} {!! Form::select('is_active',array(1=>'Active', 0=> 'Not Active'), null, ['class'=>'form-control'])
			!!}
		</div>
		<div class="form-group">
			{!! Form::label('password', 'Password') !!} {!! Form::password('password', ['class'=>'form-control']) !!}
		</div>
		<div class="form-group">
			{!! Form::submit('Update User', ['class'=>'btn btn-primary']) !!}
		</div>
    {!! Form::close() !!} 
    {!! Form::open(['method'=>'DELETE', 'action'=>['AdminUsersController@destroy', $user->id], 'files'=>true]) !!}
		<div class="form-group">
			{!! Form::submit('Delete User', ['class'=>'btn btn-danger']) !!}
		</div>
		{!! Form::close() !!}
	</div>
</div>
<div class="row">
	@include('includes.form_error')
</div>
@endsection