@extends('layouts.admin') @section('content') @if(Session::has('deleted_category'))
<p class="alert alert-danger">{{ session('deleted_category') }}</p>
@endif
<h1>Categories</h1>
<div class="col-sm-6">
	{!! Form::open(['method'=>'POST', 'action'=>'AdminCategoriesController@store']) !!}
	<div class="form-group">
		{!! Form::label('name', 'Name') !!} {!! Form::text('name', null, ['class'=>'form-control']) !!}
	</div>
	<div class="form-group">
		{!! Form::submit('Create Category', ['class'=>'btn btn-primary']) !!}
	</div>
	{!! Form::close() !!} @include('includes.form_error')
</div>
<div class="col-sm-6">
	<table class="table">
		<thead>
			<tr>
				<th>Id</th>
				<th>Name</th>
				<th>Created</th>
				<th>Updated</th>
			</tr>
		</thead>
		<tbody>
			@if($categories) @foreach($categories as $category)
			<tr>
				<td>{{$category->id}}</td>
				<td><a href="{{ route('categories.edit', $category->id) }}">{{ $category->name}}</a></td>
				<td>
					@if($category->created_at) {{$category->created_at->diffForHumans()}} @endif
				</td>
				<td>
					@if($category->updated_at) {{$category->updated_at->diffForHumans()}} @endif
				</td>
			</tr>
			@endforeach @endif
		</tbody>
	</table>
</div>
@endsection