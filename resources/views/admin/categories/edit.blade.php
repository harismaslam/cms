@extends('layouts.admin') @section('content') @if(Session::has('deleted_category'))
<p class="alert alert-danger">{{ session('deleted_category') }}</p>
@endif
<h1>Edit Category</h1>
<div class="col-sm-6">
	{!! Form::model($category, ['method'=>'PATCH', 'action'=>['AdminCategoriesController@update', $category->id]]) !!}
	<div class="form-group">
		{!! Form::label('name', 'Name') !!} {!! Form::text('name', null, ['class'=>'form-control']) !!}
	</div>
	<div class="form-group">
		{!! Form::submit('Update Category', ['class'=>'btn btn-primary col-sm-6']) !!}
	</div>
	{!! Form::close() !!} {!! Form::open(['method'=>'DELETE', 'action'=>['AdminCategoriesController@destroy', $category->id]]) !!}
	<div class="form-group">
		{!! Form::submit('Delete Category', ['class'=>'btn btn-danger col-sm-6']) !!}
	</div>
	{!! Form::close() !!} @include('includes.form_error') @endsection
</div>