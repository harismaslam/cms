@extends('layouts.admin') @section('content')
<h1>Photos</h1>
<table class="table">
	<thead>
		<tr>
			<th>Id</th>
			<th>Photo</th>
			<th>Created</th>
			<th>&nbsp;</th>
		</tr>
	</thead>
	<tbody>
		@if($photos) @foreach($photos as $photo)
		<tr>
			<td>{{$photo->id}}</td>
			<td>@if($photo->file)
				<img height="50" src="{{ asset($photo->file) }}" />@endif</td>
			<td>
				@if($photo->created_at) {{$photo->created_at->diffForHumans()}} @endif
			</td>
			<td>
					{!! Form::close() !!} {!! Form::open(['method'=>'DELETE', 'action'=>['AdminMediasController@destroy', $photo->id]])
					!!}
					<div class="form-group">
						{!! Form::submit('Delete', ['class'=>'btn btn-danger']) !!}
					</div>
					{!! Form::close() !!}
			</td>
		</tr>
		@endforeach @endif
	</tbody>
</table>
@endsection