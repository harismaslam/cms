@extends('layouts.admin') @section('content')
<h1>Comments</h1>
<table class="table">
	<thead>
		<tr>
			<th>Id</th>
			<th>Author</th>
			<th>Email</th>
			<th>Post</th>
      <th>Body</th>
      <th>Approve/Unapprove</th>
      <th>Delete</th>
		</tr>
	</thead>
	<tbody>
		@if($comments) @foreach($comments as $comment)
		<tr>
			<td>{{$comment->id}}</td>
			<td>{{ $comment->author}}</td>
			<td>{{ $comment->email}}</td>
			<td><a href="{{ route('view.post', $comment->post->id) }}">View Post</a></td>
      <td>{{ str_limit($comment->body, 30) }}</td>
      <td>
        @if($comment->is_active)
        {!! Form::open(['method'=>'PATCH', 'action'=>['PostCommentsController@update', $comment->id]])
        !!}
        {!! Form::hidden('is_active', 0) !!}
        <div class="form-group">
          {!! Form::submit('UnApprove', ['class'=>'btn btn-info']) !!}
        </div>
        {!! Form::close() !!}
        @else
        {!! Form::open(['method'=>'PATCH', 'action'=>['PostCommentsController@update', $comment->id]])
        !!}
        {!! Form::hidden('is_active', 1) !!}
        <div class="form-group">
          {!! Form::submit('Approve', ['class'=>'btn btn-success']) !!}
        </div>
        {!! Form::close() !!}
        @endif
      </td>
      <td>
      {!! Form::open(['method'=>'DELETE', 'action'=>['PostCommentsController@destroy', $comment->id]])
		!!}
		<div class="form-group">
			{!! Form::submit('Delete Post', ['class'=>'btn btn-danger']) !!}
		</div>
		{!! Form::close() !!}
      </td>
		</tr>
		@endforeach @endif
	</tbody>
</table>
@endsection